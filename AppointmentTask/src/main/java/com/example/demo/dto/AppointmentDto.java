package com.example.demo.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.example.demo.entity.Attendees;

public class AppointmentDto {

	private Long managerId;
	
	private LocalDateTime time;
	
//	private Long developerId[];
	
	
//	private List<Attendees> attendees;

//	private List<Long> developerId;
	private ArrayList<Long> developerId;
	
	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public ArrayList<Long> getDeveloperId() {
		return developerId;
	}

	public void setDeveloperId(ArrayList<Long> developerId) {
		this.developerId = developerId;
	}

//	public int getDeveloperId() {
//		return developerId;
//	}
//
//	public void setDeveloperId(int developerId) {
//		this.developerId = developerId;
//	}

//	public List<Attendees> getAttendees() {
//		return attendees;
//	}
//
//	public void setAttendees(List<Attendees> attendees) {
//		this.attendees = attendees;
//	}

//	public Long[] getDeveloperId() {
//		return developerId;
//	}
//
//	public void setDeveloperId(Long[] developerId) {
//		this.developerId = developerId;
//	}
	
	
	
}
