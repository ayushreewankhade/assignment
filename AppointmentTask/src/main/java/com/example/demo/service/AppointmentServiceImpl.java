package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.dto.AppointmentDto;
import com.example.demo.dto.AttendeesDto;
import com.example.demo.dto.IAttendeeListDto;
import com.example.demo.entity.Appointment;
import com.example.demo.entity.Attendees;
import com.example.demo.entity.UserEntity;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.repo.AppointmentRepo;
import com.example.demo.repo.AttendeesRepo;
import com.example.demo.repo.BlockRepo;
import com.example.demo.repo.UserRepository;
import com.example.demo.util.Pagination;



@Service
public class AppointmentServiceImpl {
	
	@Autowired
	private AppointmentRepo appointmentRepo;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AttendeesRepo attendeesRepo;
	
	@Autowired
	BlockRepo blockRepo;

	public String createAppointment(AppointmentDto create, String username){ //Long id,
		UserEntity user = userRepository.findByEmail(username);
		Appointment appointment= new Appointment();
		appointment.setManagerId((long) user.getId());
		appointment.setTime(create.getTime());
//		appointment.setAttendees(create.getAttendees());
		
		 appointmentRepo.save(appointment);
		
		//ArrayList<IAttendeeListDto> attendee= attendeesRepo.findByDeveloperId( IAttendeeListDto.class);
	    //ArrayList<Integer> developerId= new ArrayList<>();
		 Appointment appointment1= appointmentRepo.findByIdAndIsActiveTrue(appointmentRepo.save(appointment).getId());
			
		for(Long i : create.getDeveloperId()) {
			if(blockRepo.findByBlockedByAndBlocked( i, appointment.getManagerId()) !=null){
		          return  "you cannot create appointment with this userId =" +i;
		                    
		        }
			Attendees attendees= new Attendees();
		attendees.setDeveloperId(i);
		attendees.setAppointment(appointment1);
//		attendees.setAppointment_id(appointmentRepo.save(appointment).getId());
		attendeesRepo.save(attendees);
		
		}
		return "appointment created";
	}
	
//	public IMasterDetailDto fetchById(Long id) throws ResourceNotFoundException {
//		IMasterDetailDto userDetail = appointmentRepositories.findByIdAndIsActiveTrue(id, IMasterDetailDto.class)
//				.orElseThrow(() -> new ResourceNotFoundException("Appointment Not Found"));
//		return userDetail;
//	}

	 public List<Appointment> fetchByUserId(String username) { //int pageNo, int pageSize,
	  	 	UserEntity user = userRepository.findByEmail(username);
	  	 	//Pageable paging = PageRequest.of(pageNo, pageSize); //,new Sort("time", Sort.Direction.DESC)
	  	 	//Pageable paging= new Pagination().getPagination(pageNo, pageSize);
	  	 	List<Appointment> result= appointmentRepo.findByUserEntity(user.getId()); //paging,
	  	    return result;
	  	    
	  	 	
	  }


	
	
      public List<Attendees> findAttendeesAppointment(int pageNo, int pageSize,String username) {
	 	UserEntity user = userRepository.findByEmail(username);
	 	//Pageable paging = PageRequest.of(pageNo, pageSize); //,new Sort("time", Sort.Direction.DESC)
	 	Pageable paging= new Pagination().getPagination(pageNo, pageSize);
	 	Page<Attendees> pagedResult= attendeesRepo.findByDeveloperId(paging, (long) user.getId());
	    return pagedResult.toList();
	    

}
      
      public List<Appointment> findManagerAppointment(int pageNo, int pageSize,String username) {
  	 	UserEntity user = userRepository.findByEmail(username);
  	 	//Pageable paging = PageRequest.of(pageNo, pageSize); //,new Sort("time", Sort.Direction.DESC)
  	 	Pageable paging= new Pagination().getPagination(pageNo, pageSize);
  	 	Page<Appointment> pagedResult= appointmentRepo.findByManagerIdOrderByTimeAsc(paging, (long) user.getId());
  	    return pagedResult.toList();
  	    
  	 	
  }
      
  	 public Attendees updateResponse(AttendeesDto attendeesDto, int id, String username) {
  		UserEntity user = userRepository.findByEmail(username);
		Attendees  attendee =this.attendeesRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("appointment NOT Found "+id));
		attendee.setDeveloperId((long) user.getId());
		attendee.setResponse(attendeesDto.isResponse());
		return attendeesRepo.save(attendee);

	}
  	 
  	public void deleteAppointment(Long id) {
		this.appointmentRepo.findById(id)
		.orElseThrow( () -> new ResourceNotFoundException("appointment not found" +id));
		this.appointmentRepo.deleteById(id);

	}
}