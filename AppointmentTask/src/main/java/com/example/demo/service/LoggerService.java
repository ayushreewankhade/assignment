package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.LoggerDto;
import com.example.demo.entity.LoggerEntity;
import com.example.demo.entity.UserEntity;
import com.example.demo.repo.LoggerRepository;

@Service
public class LoggerService {

	@Autowired
	private LoggerRepository loggerRepository;

		
		public void createLogger(LoggerDto loggerDto, UserEntity jwtuser) {
			LoggerEntity logger=new LoggerEntity();
			//logger.setId(user);
			logger.setToken(loggerDto.getToken());
			logger.setExpireAt(loggerDto.getExpireAt());
			loggerRepository.save(logger);

		}
}
