package com.example.demo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.entity.UserEntity;
import com.example.demo.repo.UserRepository;

@Service
public class UserService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private RolePermissionImpl rolePermissionImpl;
	
	 @Autowired
	private PasswordEncoder bcryptEncoder;
	
  
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity user = userRepo.findUserByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("email Not found" + email);
        }
        return new User(user.getEmail(), user.getPassword(),getAuthority(user));
    }
    
	private ArrayList< SimpleGrantedAuthority> getAuthority(UserEntity user) {
		ArrayList<SimpleGrantedAuthority>authorities=new ArrayList<>();
		
		if((user.getId() + "permission") != null) {
			ArrayList<SimpleGrantedAuthority>authorities1=new ArrayList<>();
			
			System.out.println("auth1233"+authorities1);
			
			
			ArrayList<String> permissions= this.rolePermissionImpl.getPermissionByUserId(user.getId()); 
			

			System.out.println("permission"+permissions);
			
			
			
			permissions.forEach(e -> {authorities1.add(new SimpleGrantedAuthority("ROLE_"+e));
	
			});
    		authorities=authorities1;
	


	}
		System.out.println("authorites>>>>>"+authorities);
		return authorities;
	}
	
	public Boolean comparePassword(String password, String hashPassword) 
	{
		return bcryptEncoder.matches(password, hashPassword);
	}

	public UserEntity FindByEmail(String usernameOrEmail) {
	
		UserEntity user =this.userRepo.findByEmail(usernameOrEmail);
		return  user;

	}
	
	public UserEntity fingById(int id) {
		
		UserEntity user =this.userRepo.getReferenceById(id);
		return  user;

	}
	
}
