package com.example.demo.service;

import java.util.ArrayList;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.AssignRole;
import com.example.demo.entity.RoleEntity;
import com.example.demo.entity.UserEntity;
import com.example.demo.entity.UserRoleEntity;
import com.example.demo.entity.UserRoleId;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.repo.RoleRepo;
import com.example.demo.repo.UserRepository;
import com.example.demo.repo.UserRoleRepo;

@Service
public class UserRoleServiceImpl  {

	@Autowired
	private UserRepository userRepo;


	@Autowired
	private UserRoleRepo userRoleRepo;

	@Autowired
	private RoleRepo roleReporsitory;
	@Autowired
	ModelMapper mapper;

	
	public void addUserToRole(AssignRole assignRole) {

			ArrayList<UserRoleEntity>roles=new ArrayList<>();
			UserEntity user=this.userRepo.findById(assignRole.getUserId()); //.orElseThrow(() -> new ResourceNotFoundException("User NOT Found "));
			RoleEntity role=this.roleReporsitory.findById(assignRole.getRoleId()).orElseThrow(() -> new ResourceNotFoundException("ROLE NOT Found "));
			UserRoleId uri = new UserRoleId(user, role);
			UserRoleEntity ure = new UserRoleEntity();
			ure.setTask(uri);
			roles.add(ure);
			userRoleRepo.saveAll(roles);
	}
	}
