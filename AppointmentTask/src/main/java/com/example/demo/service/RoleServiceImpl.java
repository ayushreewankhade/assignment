package com.example.demo.service;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.RoleDto;
import com.example.demo.entity.RoleEntity;
import com.example.demo.repo.RoleRepo;
import com.example.demo.repo.UserRepository;

@Transactional
@Service("roleServiceImpl")
public class RoleServiceImpl {



	@Autowired
	RoleRepo roleReporsitory;
	@Autowired
	UserRepository userRepo;


	@Autowired
	ModelMapper modelMapper;


	
	public RoleEntity addRoles(RoleDto roleDto) {
		RoleEntity roleEntity=new RoleEntity();
		roleEntity.setRoleName(roleDto.getRoleName());
		return roleReporsitory.save(roleEntity);


	}
}