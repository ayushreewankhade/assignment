package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.PermissionRequestDto;
import com.example.demo.entity.PermissionEntity;
import com.example.demo.repo.PermissionRepository;

@Service
public class PermissionServiceImpl  {

	@Autowired
	private PermissionRepository permissionRepository;
	
	public void addPermission(PermissionRequestDto permissionRequestDto) {
	
		
		PermissionEntity permissionEntity=new PermissionEntity();
		permissionEntity.setActionName(permissionRequestDto.getActionName());
		permissionEntity.setBaseUrl(permissionRequestDto.getBaseUrl());
		permissionEntity.setDescription(permissionRequestDto.getDescription());
		permissionEntity.setMethod(permissionRequestDto.getMethod());
		permissionEntity.setPath(permissionRequestDto.getPath());
		permissionRepository.save(permissionEntity);
		return;
	}
}
