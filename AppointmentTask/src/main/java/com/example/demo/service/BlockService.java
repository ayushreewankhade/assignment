package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.ApiResponse;
import com.example.demo.entity.BlockList;
import com.example.demo.entity.UserEntity;
import com.example.demo.repo.BlockRepo;
import com.example.demo.repo.UserRepository;

@Service
public class BlockService {
	
	@Autowired
	BlockRepo blockRepo;
	
	@Autowired
	UserRepository userRepository;
	
	public String addBlock(BlockList block, String username) {
		UserEntity user = userRepository.findByEmail(username);
		if(blockRepo.findByBlockedByAndBlocked( (long) user.getId(), block.getBlocked()) !=null) {
			return "you have already blocked this user = " +block.getBlocked();
		}
		BlockList blocklist= new BlockList();
		blocklist.setBlockedBy((long) user.getId());
		blocklist.setBlocked(block.getBlocked());
		 blockRepo.save(blocklist);
		 return "user blocked";
	}

}
