package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.IPermissionIdList;
import com.example.demo.entity.AssignPermission;
import com.example.demo.entity.PermissionEntity;
import com.example.demo.entity.RoleEntity;
import com.example.demo.entity.RolePermissionEntity;
import com.example.demo.entity.RolePermissionId;
import com.example.demo.entity.UserRoleEntity;
import com.example.demo.repo.PermissionRepository;
import com.example.demo.repo.RolePermissionRepo;
import com.example.demo.repo.RoleRepo;
import com.example.demo.repo.UserRoleRepo;

@Service
public class RolePermissionImpl {


	@Autowired
	private RoleRepo roleRepo;

	@Autowired
	private PermissionRepository permissionRepository;

	@Autowired
	private RolePermissionRepo rolePermissionRepo;

	@Autowired
	private UserRoleRepo userRoleRepo;

	public void addPermissionToRole(AssignPermission assignPermission) {
		ArrayList<RolePermissionEntity>list =new ArrayList<>();
		RoleEntity roleEntity=this.roleRepo.findById(assignPermission.getRoleId()).get();
		PermissionEntity permissionEntity=this.permissionRepository.findById(assignPermission.getPerId()).get();
		RolePermissionEntity permissionEntity2=new RolePermissionEntity();
		RolePermissionId rolePermissionId=new RolePermissionId(roleEntity, permissionEntity);
		permissionEntity2.setPk(rolePermissionId);
		list.add(permissionEntity2);
		this.rolePermissionRepo.saveAll(list);


	}


	public List<RolePermissionEntity> getAllRolePermission() {

		return rolePermissionRepo.findAll();
	}

	public  ArrayList<String> getPermissionByUserId(Integer userId){
		//ArrayList<RoleIdListDto>roleIdListDtos=this.userRoleRepo.findByTaskUserId(userId, RoleIdListDto.class);
		ArrayList<UserRoleEntity>roleIdListDtos=this.userRoleRepo.getRoleOfUser(userId);
		System.out.println("check roleslist"+roleIdListDtos);
		ArrayList<Integer>list=new ArrayList<>();
		for(int i=0; i< roleIdListDtos.size(); i++) {
			list.add(roleIdListDtos.get(i).getTask().getRole().getId());
		}

		List<IPermissionIdList> rolesPermission = this.rolePermissionRepo.findPkPermissionByPkRoleIdIn(list, IPermissionIdList.class);
		ArrayList<String> permissions = new ArrayList<>();
System.out.println("permission1223"+permissions);
		for (IPermissionIdList element : rolesPermission) {

			permissions.add(element.getPkPermissionActionName());
			System.out.println("permission1223"+permissions);

		}
		System.out.println("permission"+ permissions);
		return permissions;

	}


}
