package com.example.demo.repo;


import java.util.ArrayList;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.IAttendeeListDto;
import com.example.demo.entity.Attendees;

public interface AttendeesRepo extends JpaRepository<Attendees, Integer> {

	Page<Attendees> findByDeveloperId(Pageable paging,Long id);
	
	//Page<Attendees> findByDeveloperIdOrderByTimeAsc(Pageable paging,Long id);
	
	
	
	//ArrayList<IAttendeeListDto> findByDeveloperId( Class<IAttendeeListDto> IAttendeeListDto);

}
