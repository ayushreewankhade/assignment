package com.example.demo.repo;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.BlockList;

@Repository
public interface BlockRepo extends JpaRepository<BlockList, Long>{

	BlockList findUserByBlockedBy(Long id);
	
	BlockList findUserByBlocked(Long id);
	
	BlockList findByBlockedByAndBlocked( Long  blockedBy, Long blocked );
}
