package com.example.demo.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.Appointment;

public interface AppointmentRepo extends JpaRepository<Appointment, Long> {

	Page<Appointment> findByManagerId(Pageable paging,long id);
	
	List<Appointment> findByUserEntity(int id);
	
	Page<Appointment> findByManagerIdOrderByTimeAsc(Pageable paging,long id);
	
	//@Modifying
	//@Transactional
	@Query(value= "SELECT * FROM Appointment ORDER BY time ASC", nativeQuery=true)
	List<Appointment> findAllByOrderByTimeAsc(String username);

	Appointment findByIdAndIsActiveTrue(Long id);



	//Appointment save(Appointment appointment, int id);
}
