package com.example.demo.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Appointment;
import com.example.demo.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

	    List<UserEntity> findAll();

	    UserEntity findByEmail(String email);

	    UserEntity findUserByEmail(String email);

	    UserEntity findByToken(String token);
	    
	    UserEntity findById(int id);
	    
	    List<Appointment> findById(String username);

		UserEntity findById(Long i);

	
	}

