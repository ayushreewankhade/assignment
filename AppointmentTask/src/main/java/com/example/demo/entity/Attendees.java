package com.example.demo.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Attendees {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	
    private int attendees_id;
	
//	private Long appointment_id;
	
	@Column(name = "developer_id")
	private Long developerId;
		
	private boolean response;

	@ManyToOne
	@JoinColumn(name = "appointment_id")
	private Appointment appointment;

	public int getId() {
		return attendees_id;
	}

	public void setId(int attendees_id) {
		this.attendees_id = attendees_id;
	}

	public Appointment getAppointment() {
		return appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}



	public Attendees() {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean isResponse() {
		return response;
	}

	public void setResponse(boolean response) {
		this.response = response;
	}

	public Long getDeveloperId() {
		return developerId;
	}

	public void setDeveloperId(Long developerId) {
		this.developerId = developerId;
	}


	
	
	
}
