package com.example.demo.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Appointment {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="appointment_id")
    private Long id;
	
	private Long managerId;
	
	@Column(columnDefinition = "TIMESTAMP")
	@CreationTimestamp
	private LocalDateTime creationTime;
	
	@Column(columnDefinition = "TIMESTAMP")
	private LocalDateTime time;
	
	@Column(name = "is_active")
	private Boolean isActive = true;

	@OneToMany(mappedBy="appointment")
    private List<Attendees> attendees;
	
	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private UserEntity userEntity;


	public Long getId() {
		return id;
	}

	public void setId(Long appointment_id) {
		this.id = appointment_id;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	public LocalDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(LocalDateTime creationTime) {
		this.creationTime = creationTime;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public List<Attendees> getAttendees() {
		return attendees;
	}

	public void setAttendees(List<Attendees> attendees) {
		this.attendees = attendees;
	}

	public Appointment(Long id, long managerId, LocalDateTime creationTime, LocalDateTime time,
			List<Attendees> attendees) {
		super();
		this.id =id;
		this.managerId = managerId;
		this.creationTime = creationTime;
		this.time = time;
		this.attendees = attendees;
	}

	public Appointment() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Appointment [id=" + id + ", managerId=" + managerId + ", creationTime=" + creationTime + ", time="
				+ time + ", attendees=" + attendees + "]";
	}
	
	
	
}
