package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class BlockList {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//@OneToOne(fetch=FetchType.LAZY)
	private Long blockedBy;
	
	//@OneToOne(fetch=FetchType.LAZY)
	private Long blocked;



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public Long getBlockedBy() {
		return blockedBy;
	}



	public void setBlockedBy(Long blockedBy) {
		this.blockedBy = blockedBy;
	}



	public Long getBlocked() {
		return blocked;
	}



	public void setBlocked(Long blocked) {
		this.blocked = blocked;
	}



	public BlockList() {
		super();
		// TODO Auto-generated constructor stub
	}



	public BlockList(Long id, Long blockedBy, Long blocked) {
		super();
		this.id = id;
		this.blockedBy = blockedBy;
		this.blocked = blocked;
	}
	
	

}
