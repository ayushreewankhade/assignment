package com.example.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ApiResponse;
import com.example.demo.dto.AppointmentDto;
import com.example.demo.dto.ErrorResponseDto;
import com.example.demo.dto.SuccessResponseDto;
import com.example.demo.entity.Appointment;
import com.example.demo.entity.Attendees;
import com.example.demo.entity.BlockList;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.repo.AppointmentRepo;
import com.example.demo.repo.AttendeesRepo;
import com.example.demo.repo.BlockRepo;
import com.example.demo.repo.UserRepository;
import com.example.demo.service.AppointmentServiceImpl;
import com.example.demo.util.JwtUtil;

@RestController
@RequestMapping("/appointment")
public class AppointmentController {
	
	@Autowired
	AppointmentServiceImpl appointmentServiceImpl;
	
	@Autowired
	AppointmentRepo appointmentRepo;
	
	@Autowired
	AttendeesRepo attendeesRepo;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	BlockRepo blockRepo;
	
	@Autowired
	JwtUtil jwtUtil;


	@PreAuthorize("hasRole('addAppointment')")
	@PostMapping("/createAppointment")
	public ResponseEntity<?> addAppointment(@Valid @RequestBody AppointmentDto appointmentDto,@RequestHeader("Authorization") String token){ //@PathVariable(value = "id") Long id,
		 String temp = token.split(" ")[1];
		  String username = jwtUtil.extractUsername(temp);
		String appointment= this.appointmentServiceImpl.createAppointment(appointmentDto,username); //,id
		return new ResponseEntity<>(appointment, HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasRole('getAppointmentByManagerId')")
	@GetMapping("/fetchAppointmentByManagerid")
		public ResponseEntity<List<Appointment>> getAppointmentByManagerId(@RequestParam (required = false,defaultValue = "1") int pageNo, 
				@RequestParam(required = false,defaultValue = "10") int pageSize,@RequestHeader("Authorization") String token){
		 String temp = token.split(" ")[1];
		  String username = jwtUtil.extractUsername(temp);
		return new ResponseEntity<List<Appointment>>(appointmentServiceImpl.findManagerAppointment(pageNo, pageSize,username),HttpStatus.OK);
	}

//	@GetMapping("/fetch")
//	public ResponseEntity<List<Appointment>> getAppointmentById(@RequestHeader("Authorization") String token){ //@RequestParam (required = false,defaultValue = "1") int pageNo, 
////	@RequestParam(required = false,defaultValue = "10") int pageSize,
//	 String temp = token.split(" ")[1];
//	  String username = jwtUtil.extractUsername(temp);
//	return new ResponseEntity<List<Appointment>>(appointmentServiceImpl.fetchByUserId(username),HttpStatus.OK); //pageNo, pageSize,
//}
	
	@PreAuthorize("hasRole('deleteAppointment')")
	@DeleteMapping("/{id}")
        public ResponseEntity<?>deleteAppointment(@PathVariable(value = "id")Long id){
	    this.appointmentServiceImpl.deleteAppointment(id);
	
	    try {
	    return new ResponseEntity<>(new SuccessResponseDto("appointment FOUND","DELETED","!!!!"),HttpStatus.FOUND);
	    }
	    catch(ResourceNotFoundException e) {
		return  new ResponseEntity<>(new ErrorResponseDto(e.getMessage(),"appointment Not Found"),HttpStatus.BAD_REQUEST);
		
	}    
    }
	
	
}
