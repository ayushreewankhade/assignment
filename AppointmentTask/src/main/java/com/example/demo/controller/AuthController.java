package com.example.demo.controller;

import java.util.Calendar;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ApiResponse;
import com.example.demo.dto.AuthenticationResponse;
import com.example.demo.dto.ErrorResponseDto;
import com.example.demo.dto.LoggerDto;
import com.example.demo.dto.LoginRequest;
import com.example.demo.dto.SignUpRequest;
import com.example.demo.entity.UserEntity;
import com.example.demo.exceptions.ApiExceptions;
import com.example.demo.repo.UserRepository;
import com.example.demo.service.LoggerService;
import com.example.demo.service.UserService;
import com.example.demo.util.JwtUtil;
import com.example.demo.util.PasswordValidator;


@RestController
@RequestMapping("/user")
public class AuthController {
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder passwordEncoder;
    
    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    UserService userService;
    
    @Autowired
    LoggerService loggerService;
    
	    @PostMapping("/signup")
	    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {

	    	//if(userRepository.findUserByEmail(signUpRequest.getPassword().matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\S+$).{8,20}$")) != false) {
	    	//	 return new ResponseEntity(new ApiResponse(false, "Enter Strong password"),
		    //               HttpStatus.BAD_REQUEST);
	    	//}
	    	String email = signUpRequest.getEmail();
			String password = signUpRequest.getPassword();

	    	if (PasswordValidator.isValid(password)) {
	        if(userRepo.findUserByEmail(signUpRequest.getEmail()) != null) {
	          return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
	                   HttpStatus.BAD_REQUEST); 
	        }
	        // Creating user's account
	        UserEntity user = new UserEntity();
	        user.setEmail(signUpRequest.getEmail());
	        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
	        userRepo.save(user);
	        return ResponseEntity.ok(new ApiResponse(true, "User registered successfully"));
	    	}	 else  {

				return ResponseEntity.ok(new ErrorResponseDto(
						"Password should have Minimum 8 and maximum 50 characters, at least one uppercase letter, one lowercase letter, one number and one special character and No White Spaces",
						"Password validation not done"));
	    	}
	    }
	    	
	    	@PostMapping("/signin")
		    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		        /*Authentication authentication = authenticationManager.authenticate(
		                new UsernamePasswordAuthenticationToken(
		                        loginRequest.getUsernameOrEmail(),
		                        loginRequest.getPassword()
		                        
		                )
		        );*/
		    	try {
		    	UserEntity user = userService.FindByEmail(loginRequest.getUsernameOrEmail());   //findByEmail(authenticationRequest.getEmail());
				System.out.println(user.getPassword());

				if (userService.comparePassword(loginRequest.getPassword(), user.getPassword())) {
		       // SecurityContextHolder.getContext().setAuthentication(authentication);
		       // UserDetails userDetails = userDetailsService.loadUserByUsername(loginRequest.getUsernameOrEmail());

		        String jwt = jwtUtil.generateToken(user);

		        LoggerDto logger = new LoggerDto(); //
				logger.setToken(jwt);
				Calendar calender = Calendar.getInstance();
				calender.add(Calendar.HOUR_OF_DAY, 5);
				logger.setExpireAt(calender.getTime());
				loggerService.createLogger(logger, user); //
				return ResponseEntity.ok(new AuthenticationResponse(jwt));
		    }
				}catch(BadCredentialsException e) {
				throw new ApiExceptions("INVALID USERNAME OR PASSWORD");

			}catch (Exception e) {
				return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(),"User Not Found"),HttpStatus.NOT_FOUND);

			}
		    	return new ResponseEntity<>(new ErrorResponseDto("Invalid PASSWORD", "CHECK PASSWORD"),HttpStatus.UNAUTHORIZED);
		    }
}
	    

