package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.AttendeesDto;
import com.example.demo.dto.ErrorResponseDto;
import com.example.demo.entity.Attendees;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.repo.AttendeesRepo;
import com.example.demo.repo.UserRepository;
import com.example.demo.service.AppointmentServiceImpl;
import com.example.demo.util.JwtUtil;

@RestController
public class AttendeesController {
	
	@Autowired
	AttendeesRepo attendeesRepo;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AppointmentServiceImpl appointmentServiceImpl;
	
	@Autowired
	JwtUtil jwtUtil;

//	@GetMapping("/getAppointmentByDeveloperId")
//	public ResponseEntity<List<Attendees>> getByDeveloperId(@RequestParam int developerId){
//		return new ResponseEntity<List<Attendees>>(attendeesRepo.findByDeveloperId(developerId),HttpStatus.OK);
//		}

@PreAuthorize("hasRole('getAppointment')")
@GetMapping("/getAppointmentByDeveloperId")
public ResponseEntity<List<Attendees>> getAppointment(@RequestParam (required = false,defaultValue = "1") int pageNo, 
		@RequestParam(required = false,defaultValue = "10") int pageSize,@RequestHeader("Authorization") String token) {

	  String temp = token.split(" ")[1];
	  String username = jwtUtil.extractUsername(temp);
    return new ResponseEntity<>( appointmentServiceImpl.findAttendeesAppointment(pageNo, pageSize,username),HttpStatus.OK);

}

@PreAuthorize("hasRole('updateDevResponse')")
@PutMapping("/developerResponse/{id}")
public ResponseEntity<?> updateDevResponse(@RequestBody AttendeesDto attendeesDto ,@PathVariable(value = "id")Integer id,@RequestHeader("Authorization") String token){
	 String temp = token.split(" ")[1];
	  String username = jwtUtil.extractUsername(temp);
	try 
	{
		this.appointmentServiceImpl.updateResponse(attendeesDto, id,username);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}catch(ResourceNotFoundException e)
	{
		return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "user not found"),HttpStatus.BAD_REQUEST);

	}
}
}