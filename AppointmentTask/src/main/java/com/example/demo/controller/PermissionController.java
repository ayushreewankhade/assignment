package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.PermissionRequestDto;
import com.example.demo.service.PermissionServiceImpl;


@RestController
@RequestMapping("/permission")
public class PermissionController {


	public PermissionController() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Autowired
	private PermissionServiceImpl permissionServiceImpl;


	@PostMapping
	public ResponseEntity<?> addPermissions( @RequestBody PermissionRequestDto permissionRequestDto){
		this.permissionServiceImpl.addPermission(permissionRequestDto);
		return new ResponseEntity<>("Success",HttpStatus.OK);

	}
}
