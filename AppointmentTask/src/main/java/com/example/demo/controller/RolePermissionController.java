package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.AssignPermission;
import com.example.demo.service.RolePermissionImpl;

@RestController
@RequestMapping("role-permission")
public class RolePermissionController {
	
	@Autowired
	private RolePermissionImpl rolePermissionService;
	
	
	@PostMapping
	public ResponseEntity<?>addPermissionToRole(@RequestBody AssignPermission assignPermission){
		this.rolePermissionService.addPermissionToRole(assignPermission);
		System.out.println("ID per"+assignPermission.getPerId());
		System.out.println("ID role"+assignPermission.getRoleId());
		return ResponseEntity.ok(HttpStatus.CREATED);
		
	}
}
