package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.SuccessResponseDto;
import com.example.demo.entity.AssignRole;
import com.example.demo.repo.RoleRepo;
import com.example.demo.repo.UserRepository;
import com.example.demo.service.RoleServiceImpl;
import com.example.demo.service.UserRoleServiceImpl;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/user_role")
public class UserRoleController {

	@Autowired
	RoleServiceImpl roleService;

	@Autowired
	UserRepository userRepo;

	@Autowired
	UserService userService;
	
	@Autowired
	UserRoleServiceImpl userRoleService;

	@Autowired
	RoleRepo roleReporsitory;
	
	@PostMapping("/role")
	public ResponseEntity<?> addUserRole(@RequestBody AssignRole assignRole ) throws Exception
	{
		try {

			this.userRoleService.addUserToRole(assignRole);

			return new ResponseEntity<>(new SuccessResponseDto("add UserToRoles", "check", null),HttpStatus.OK);
		}catch(Exception e) 
		{
			return new ResponseEntity<>("Already.existis userRole",HttpStatus.BAD_REQUEST);
		}
	}
}
