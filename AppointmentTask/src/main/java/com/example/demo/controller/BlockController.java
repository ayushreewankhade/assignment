package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.BlockList;
import com.example.demo.service.BlockService;
import com.example.demo.util.JwtUtil;

@RestController
public class BlockController {

	@Autowired
	BlockService blockService;
	
	@Autowired
	JwtUtil jwtUtil;
 
	@PreAuthorize("hasRole('blockUser')")
	@PostMapping("/block")
	public ResponseEntity<?> blockUser(@Valid @RequestBody BlockList blockList,@RequestHeader("Authorization") String token){
		String temp = token.split(" ")[1];
		  String username = jwtUtil.extractUsername(temp);
		String block= this .blockService.addBlock(blockList,username);
		return new ResponseEntity<>(block, HttpStatus.CREATED);
	}
	

}
