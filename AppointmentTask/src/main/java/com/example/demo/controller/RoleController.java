package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.RoleDto;
import com.example.demo.entity.RoleEntity;
import com.example.demo.repo.RoleRepo;
import com.example.demo.service.RoleServiceImpl;

@RestController
@RequestMapping("/role")
public class RoleController {


	public RoleController() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Autowired
	private RoleServiceImpl roleService;
	@Autowired
	private RoleRepo   roleReporsitory;

	@PostMapping()
	public ResponseEntity<?> addRole(@Valid @RequestBody RoleDto roleDto) {
		RoleEntity roleEntity=this.roleService.addRoles(roleDto);

		return  new ResponseEntity<>(roleEntity,HttpStatus.CREATED);		
	}
}